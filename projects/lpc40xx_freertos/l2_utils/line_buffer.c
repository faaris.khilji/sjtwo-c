#include "line_buffer.h"
#include <stdio.h>

static void adjust_buffer(line_buffer_s *buffer, int shift_amt) {
  if (buffer != NULL) {
    char *buffer_mem = ((char *)(buffer->memory));
    for (int i = 0; i < shift_amt; i++) {
      for (int j = 0; j < buffer->max_size; j++) {
        if (j + 1 < buffer->max_size) {
          buffer_mem[j] = buffer_mem[j + 1];
        }
      }
    }
    buffer->write_index = buffer->write_index - shift_amt;
  }
}

void line_buffer__init(line_buffer_s *buffer, void *memory, size_t size) {
  if (buffer != NULL && memory != NULL && size != 0) {
    buffer->max_size = size;
    buffer->memory = memory;
    buffer->write_index = 0;
  }
}

bool line_buffer__add_byte(line_buffer_s *buffer, char byte) {
  bool success = false;
  if (buffer != NULL && buffer->memory != NULL && buffer->write_index < buffer->max_size) {
    ((char *)(buffer->memory))[buffer->write_index] = byte;
    buffer->write_index++;
    success = true;
  }
  return success;
}

bool line_buffer__remove_line(line_buffer_s *buffer, char *line, size_t line_max_size) {
  if (buffer != NULL && buffer->memory != NULL && line_max_size > 0) {
    char *buffer_mem = ((char *)(buffer->memory)); // ptr to buffer memory
    int i = 0;
    while (i < line_max_size) {
      if (buffer_mem[i] == '\n') // reached end of line
      {
        adjust_buffer(buffer, i + 1); // shift array left and adjust write idx
        line[i] = '\0';               // terminate the string
        return true;
      }
      if (i == buffer->write_index && buffer_mem[i] != '\n' && i != buffer->max_size - 1) // no line present
      {
        line[0] = '\0';
        return false;
      }
      if (i == buffer->max_size - 1 && buffer_mem[i] != '\n') {
        line[i] = '\0';
        printf("LINE_BUF FULL!%s\n", line);
        adjust_buffer(buffer, i + 1);
        return true;
      }
      line[i] = buffer_mem[i];
      i++;
    }
    line[i - 1] = '\0';
    return true;
  }
  return false;
}