#include "my_queue_v2.h"
#include <stdio.h>
#include <string.h>

void queue__init_v2(queue_s_v2 *queue, void *static_memory_for_queue, size_t static_memory_size_in_bytes) {
  if (queue != NULL) {
    queue->static_memory_for_queue = static_memory_for_queue;
    queue->static_memory_size_in_bytes = static_memory_size_in_bytes;
    queue->item_count = 0;
    queue->read_idx = 0;
    queue->write_idx = 0;
  }
}

/// @returns false if the queue is full
bool queue__push_v2(queue_s_v2 *queue, uint8_t push_value) {
  bool success = false;
  if (queue != NULL && queue->item_count < queue->static_memory_size_in_bytes &&
      queue->static_memory_for_queue != NULL) {
    queue->static_memory_for_queue[queue->write_idx] = push_value;
    queue->write_idx = (queue->write_idx + 1) % queue->static_memory_size_in_bytes;
    success = true;
    queue->item_count++;
  }
  return success;
}

/// @returns false if the queue was empty
bool queue__pop_v2(queue_s_v2 *queue, uint8_t *pop_value) {
  bool success = false;
  if (queue != NULL && queue->item_count > 0 && queue->static_memory_for_queue != NULL) {
    *pop_value = queue->static_memory_for_queue[queue->read_idx];
    queue->read_idx = (queue->read_idx + 1) % queue->static_memory_size_in_bytes;
    queue->item_count--;
    success = true;
  }
  return success;
}

size_t queue__get_item_count_v2(const queue_s_v2 *queue) { return queue->item_count; }