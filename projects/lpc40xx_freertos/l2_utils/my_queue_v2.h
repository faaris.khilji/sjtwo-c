#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/* In this part, the queue memory is statically defined
 * by the user and provided to you upon queue__init()
 */
typedef struct {
  uint8_t *static_memory_for_queue;
  size_t static_memory_size_in_bytes;
  uint8_t read_idx;
  uint8_t write_idx;
  uint8_t item_count;
  // TODO: Add more members as needed
} queue_s_v2;

/* Initialize the queue with user provided static memory
 * @param static_memory_for_queue This memory pointer should not go out of scope
 *
 * @code
 *   static uint8_t memory[128];
 *   queue_s_v2 queue;
 *   queue__init(&queue, memory, sizeof(memory));
 * @endcode
 */
void queue__init_v2(queue_s_v2 *queue, void *static_memory_for_queue, size_t static_memory_size_in_bytes);

/// @returns false if the queue is full
bool queue__push_v2(queue_s_v2 *queue, uint8_t push_value);

/// @returns false if the queue was empty
bool queue__pop_v2(queue_s_v2 *queue, uint8_t *pop_value);

size_t queue__get_item_count_v2(const queue_s_v2 *queue);