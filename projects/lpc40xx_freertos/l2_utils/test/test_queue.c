#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "my_queue.h"

void setUp(void) {}

void tearDown(void) {}

void test_comprehensive(void) {
  const size_t max_queue_size = 100; // Change if needed
  queue_s queue;
  queue__init(&queue);
  for (size_t item = 0; item < max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }

  // Should not be able to push anymore
  TEST_ASSERT_FALSE(queue__push(&queue, 123));
  TEST_ASSERT_EQUAL(max_queue_size, queue__get_item_count(&queue));

  // Pull and verify the FIFO order
  for (size_t item = 0; item < max_queue_size; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }

  // Test wrap-around case
  const uint8_t pushed_value = 123;
  TEST_ASSERT_TRUE(queue__push(&queue, pushed_value));
  uint8_t popped_value = 0;
  TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(pushed_value, popped_value);

  TEST_ASSERT_EQUAL(0, queue__get_item_count(&queue));
  TEST_ASSERT_FALSE(queue__pop(&queue, &popped_value));
}

void test_push(void) {
  const size_t max_queue_size = 100; // Change if needed
  queue_s queue;
  queue__init(&queue);

  // Fill up the queue and verify item count
  for (size_t item = 0; item < max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }

  // Make sure queue can't take any more items
  uint8_t item_pushed = 0;
  TEST_ASSERT_FALSE(queue__push(&queue, item_pushed));
}

void test_push_pop(void) {
  const size_t max_queue_size = 100; // Change if needed
  queue_s queue;
  queue__init(&queue);

  for (size_t item = 0; item < max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }

  for (size_t item = 0; item < max_queue_size; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }

  uint8_t popped_value = 0;
  uint8_t pushed_value = 0x1a;
  TEST_ASSERT_FALSE(queue__pop(&queue, &popped_value));
  TEST_ASSERT_TRUE(queue__push(&queue, pushed_value));
  TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(0x1a, popped_value);
}

void test_null_queue(void) {
  const size_t max_queue_size = 100; // Change if needed
  uint8_t queue_memory[max_queue_size];
  queue_s queue;
  queue__init(NULL);
  TEST_ASSERT_FALSE(queue__push(NULL, 5));
}