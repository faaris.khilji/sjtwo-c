#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "my_queue_v2.h"

void setUp(void) {}

void tearDown(void) {}

void test_comprehensive(void) {
  const size_t max_queue_size = 255; // Change if needed
  uint8_t queue_memory[max_queue_size];
  queue_s_v2 queue;
  queue__init_v2(&queue, queue_memory, sizeof(queue_memory));
  for (size_t item = 0; item < max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push_v2(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count_v2(&queue));
  }

  // Should not be able to push anymore
  TEST_ASSERT_FALSE(queue__push_v2(&queue, 123));
  TEST_ASSERT_EQUAL(max_queue_size, queue__get_item_count_v2(&queue));

  // Pull and verify the FIFO order
  for (size_t item = 0; item < max_queue_size; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop_v2(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }

  // Test wrap-around case
  const uint8_t pushed_value = 123;
  TEST_ASSERT_TRUE(queue__push_v2(&queue, pushed_value));
  uint8_t popped_value = 0;
  TEST_ASSERT_TRUE(queue__pop_v2(&queue, &popped_value));
  TEST_ASSERT_EQUAL(pushed_value, popped_value);

  TEST_ASSERT_EQUAL(0, queue__get_item_count_v2(&queue));
  TEST_ASSERT_FALSE(queue__pop_v2(&queue, &popped_value));
}

void test_push(void) {
  const size_t max_queue_size = 200; // Change if needed
  uint8_t queue_memory[max_queue_size];
  queue_s_v2 queue;
  queue__init_v2(&queue, queue_memory, sizeof(queue_memory));

  // Fill up the queue and verify item count
  for (size_t item = 0; item < max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push_v2(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count_v2(&queue));
  }

  // Make sure queue can't take any more items
  uint8_t item_pushed = 0;
  TEST_ASSERT_FALSE(queue__push_v2(&queue, item_pushed));
}

void test_push_pop(void) {
  const size_t max_queue_size = 100; // Change if needed
  uint8_t queue_memory[max_queue_size];
  queue_s_v2 queue;
  queue__init_v2(&queue, queue_memory, sizeof(queue_memory));

  for (size_t item = 0; item < max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push_v2(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count_v2(&queue));
  }

  for (size_t item = 0; item < max_queue_size; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop_v2(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }

  uint8_t popped_value = 0;
  uint8_t pushed_value = 0x1a;
  TEST_ASSERT_FALSE(queue__pop_v2(&queue, &popped_value));
  TEST_ASSERT_TRUE(queue__push_v2(&queue, pushed_value));
  TEST_ASSERT_TRUE(queue__pop_v2(&queue, &popped_value));
  TEST_ASSERT_EQUAL(0x1a, popped_value);
}

void test_null_queue_memory(void) {
  const size_t max_queue_size = 100; // Change if needed
  uint8_t queue_memory[max_queue_size];
  uint8_t queue_mem_ptr = NULL;
  queue_s_v2 queue;
  queue__init_v2(&queue, queue_mem_ptr, sizeof(queue_memory));

  TEST_ASSERT_FALSE(queue__push_v2(&queue, 5));
}

void test_null_queue(void) {
  const size_t max_queue_size = 100; // Change if needed
  uint8_t queue_memory[max_queue_size];
  queue_s_v2 queue;
  queue__init_v2(NULL, queue_memory, sizeof(queue_memory));

  TEST_ASSERT_FALSE(queue__push_v2(NULL, 5));
}