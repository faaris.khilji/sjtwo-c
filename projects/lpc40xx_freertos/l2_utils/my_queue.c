#include "my_queue.h"
#include <stdio.h>
#include <string.h>

void queue__init(queue_s *queue) {
  if (queue != NULL) {
    memset(queue, 0, sizeof(queue_s));
  }
}

/// @returns false if the queue is full
bool queue__push(queue_s *queue, uint8_t push_value) {
  bool success = false;
  if (queue != NULL && queue->item_count < 100) {
    queue->queue_memory[queue->write_idx] = push_value;
    queue->write_idx = (queue->write_idx + 1) % 100;
    success = true;
    queue->item_count++;
  }
  return success;
}

/// @returns false if the queue was empty
bool queue__pop(queue_s *queue, uint8_t *pop_value) {
  bool success = false;
  if (queue != NULL && queue->item_count > 0) {
    *pop_value = queue->queue_memory[queue->read_idx];
    queue->read_idx = (queue->read_idx + 1) % 100;
    queue->item_count--;
    success = true;
  }
  return success;
}

size_t queue__get_item_count(const queue_s *queue) { return queue->item_count; }