#include <stdio.h>

#include "board_io.h"
#include "gpio.h"
#include "message_handler.h"

static dbc_SENSOR_SONARS_s sensor_sonar_data;
static gps_coordinates_t gps_data;

dbc_SENSOR_SONARS_s message_handler__get_sensor_sonars_data(void) { return sensor_sonar_data; }

gps_coordinates_t message_handler__get_gps_data(void) { return gps_data; }

void message_handler__set_sensor_sonars_data(dbc_SENSOR_SONARS_s data) { sensor_sonar_data = data; }

void handle_sensor_sonars_message(dbc_message_header_t header, const uint8_t bytes[8]) {
  dbc_SENSOR_SONARS_s decoded_sensor_sonars = {};
  dbc_decode_SENSOR_SONARS(&decoded_sensor_sonars, header, bytes);
  sensor_sonar_data = decoded_sensor_sonars;
  gpio__reset(board_io__get_led0());
}

void handle_brake_lights_message(dbc_message_header_t header, const uint8_t bytes[8]) {
  dbc_BRAKE_LIGHTS_s decode_brake_lights = {};
  dbc_decode_BRAKE_LIGHTS(&decode_brake_lights, header, bytes);
  if (decode_brake_lights.BRAKE_LIGHTS_on) {
    gpio__reset(board_io__get_led3());
  } else {
    gpio__set(board_io__get_led3());
  }
}

void handle_gps_nav_message(dbc_message_header_t header, const uint8_t bytes[8]) {
  dbc_GPS_NAV_s decoded_gps;
  dbc_decode_GPS_NAV(&decoded_gps, header, bytes);
  gps_data.latitude = decoded_gps.GPS_NAV_latitude;
  gps_data.longitude = decoded_gps.GPS_NAV_longitude;
  // printf("%d\n", decoded_gps.GPS_NAV_latitude);
}