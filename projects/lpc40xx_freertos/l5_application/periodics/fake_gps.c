// @file: fake_gps.c
#include "fake_gps.h" // TODO: You need to create this module, unit-tests for this are optional
#include "gpio.h"

#include "uart.h"
#include "uart_printf.h"

#include "clock.h" // needed for UART initialization

// Change this according to which UART you plan to use
static uart_e gps_uart = UART__2;

void fake_gps__init(void) {
  uart__init(gps_uart, clock__get_peripheral_clock_hz(), 38400);

  gpio__construct_with_function(GPIO__PORT_0, 10, GPIO__FUNCTION_1);
  gpio__construct_with_function(GPIO__PORT_0, 11, GPIO__FUNCTION_1);

  QueueHandle_t rxq_handle = xQueueCreate(4, sizeof(char));   // Nothing to receive
  QueueHandle_t txq_handle = xQueueCreate(100, sizeof(char)); // We send a lot of data
  uart__enable_queues(gps_uart, rxq_handle, txq_handle);
}

/// TODO: You may want to be somewhat random about the coordinates that you send here
void fake_gps__run_once(void) {
  static float longitude = 0;
  static float latitude = 0;
  uart_printf(gps_uart, "$GPGGA,230612.015,%4.4f,N,%4.4f,E,0,04,5.7,508.3,M,,,,0000*13\r\n", latitude, longitude);
  longitude += 1.15; // random incrementing value
  latitude += 1.0;   // random incrementing value
}