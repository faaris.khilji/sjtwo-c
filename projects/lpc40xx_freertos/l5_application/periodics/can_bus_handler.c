//#include <stdbool.h>
#include <stdio.h>

#include "acceleration.h"
#include "can_bus.h"
#include "gpio.h"

#include "can_bus_handler.h"

static uint16_t x_data, y_data, z_data;

static gpio_s led_x;
static gpio_s led_y;
static gpio_s led_z;

void can_accel_led_turn_off_lowest_value(uint16_t acclerator_x, uint16_t acclerator_y, uint16_t acclerator_z) {
  int16_t signed_x = (int16_t)acclerator_x;
  int16_t signed_y = (int16_t)acclerator_y;
  int16_t signed_z = (int16_t)acclerator_z;

  if ((signed_x < signed_y) && (signed_x < signed_z)) {
    gpio__set(led_x);
    gpio__reset(led_y);
    gpio__reset(led_z);
  } else if ((signed_y < signed_x) && (signed_y < signed_z)) {
    gpio__reset(led_x);
    gpio__set(led_y);
    gpio__reset(led_z);
  } else if ((signed_z < signed_x) && (signed_z < signed_y)) {
    gpio__reset(led_x);
    gpio__reset(led_y);
    gpio__set(led_z);
  } else {
    gpio__set(led_x);
    gpio__set(led_y);
    gpio__set(led_z);
  }
}

static void can_handler_create_button_press_message(can__msg_t *message) {
  gpio_s switch_2 = {GPIO__PORT_0, 29};

  if (gpio__get(switch_2)) {
    message->data.qword = 0x00;
  } else {
    message->data.qword = 0xAA;
  }
}

static void can_handler_led_switch(uint32_t button_status) {
  gpio_s led_2 = {GPIO__PORT_1, 18};
  if (button_status == 0xAA) {
    gpio__set(led_2);
  } else if (button_status == 0x00) {
    gpio__reset(led_2);
    printf("Recieved button press!\n");
  }
}

bool can_handler_send_button_press(can__num_e can) {
  bool message_sent = false;

  if (can == can1 || can == can2) {
    can__msg_t can_message = {};
    can_message.msg_id = BUTTON_STATUS_MSGID;
    can_message.frame_fields.is_29bit = 0;
    can_message.frame_fields.data_len = 2;

    can_handler_create_button_press_message(&can_message);

    message_sent = can__tx(can, &can_message, 0);
  }

  return message_sent;
}

void can_handler_handle_button_press(can__num_e can) {
  if (can == can1 || can == can2) {
    can__msg_t can_message = {};
    while (can__rx(can1, &can_message, 0)) {
      if (can_message.msg_id == BUTTON_STATUS_MSGID) {
        can_handler_led_switch(can_message.data.qword);
      }
    }
  }
}

bool can_handler_send_accelerator_data(can__num_e can) {
  bool message_sent = false;
  static uint16_t x = 0;
  static uint16_t y = 1;
  static uint16_t z = 2;
  if (can == can1 || can == can2) {
    can__msg_t can_message = {};
    can_message.msg_id = ACCEL_DATA_MSGID;
    can_message.frame_fields.is_29bit = 0;
    can_message.frame_fields.data_len = 6;

    can_message.data.words[0] = x; //(uint16_t)acceleration_data.x;
    can_message.data.words[1] = y; //(uint16_t)acceleration_data.y;
    can_message.data.words[2] = z; //(uint16_t)acceleration_data.z;

#if USE_ACCEL_PHYS_SENSOR
    acceleration__axis_data_s acceleration_data;
    acceleration_data = acceleration__get_data();
    can_message.data.words[0] = (uint16_t)acceleration_data.x;
    can_message.data.words[1] = (uint16_t)acceleration_data.y;
    can_message.data.words[2] = (uint16_t)acceleration_data.z;
#endif
    x++;
    y++;
    z++;

    message_sent = can__tx(can, &can_message, 0);
  }
  return message_sent;
}

bool can_handler_handle_messages(can__num_e can) {
  bool msg_processed = false;
  if (can == can1 || can == can2) {
    can__msg_t can_message = {};
    while (can__rx(can1, &can_message, 0)) {
      if (can_message.msg_id == ACCEL_DATA_MSGID) {
        // can_handler_process_accelerator_data(&can_message);
        x_data = can_message.data.words[0];
        y_data = can_message.data.words[1];
        z_data = can_message.data.words[2];

        can_accel_led_turn_off_lowest_value(x_data, y_data, z_data);
        msg_processed = true;

      } else if (can_message.msg_id == BUTTON_STATUS_MSGID) {
        can_handler_led_switch(can_message.data.qword);
        msg_processed = true;
      }
    }
  }

  return msg_processed = true;
}

void can_handler_print_accel_data(void) { printf("Curr val (x,y,z): %d, %d, %d\n", x_data, y_data, z_data); }

static void can_led_init_gpios(void) {
  led_x = gpio__construct_as_output(GPIO__PORT_2, 3);
  led_y = gpio__construct_as_output(GPIO__PORT_1, 26);
  led_z = gpio__construct_as_output(GPIO__PORT_1, 24);
}

static void can_led_turn_off_all_leds(void) {
  gpio__set(led_x);
  gpio__set(led_y);
  gpio__set(led_z);
}

void can_accel_leds_init(void) {
  can_led_init_gpios();
  can_led_turn_off_all_leds();
}
