#pragma once

#include "can_bus.h"

#define USE_ACCEL_PHYS_SENSOR 0

#define BUTTON_STATUS_MSGID 1
#define ACCEL_DATA_MSGID 2

// handle button press message via CAN
void can_handler_handle_button_press(can__num_e can);

// send button press message via CAN
bool can_handler_send_button_press(can__num_e can);

bool can_handler_handle_messages(can__num_e can);

bool can_handler_send_accelerator_data(can__num_e can);

void can_handler_print_accel_data(void);

void can_accel_leds_init(void);

void can_accel_led_turn_off_lowest_value(uint16_t acclerator_x, uint16_t acclerator_y, uint16_t acclerator_z);
