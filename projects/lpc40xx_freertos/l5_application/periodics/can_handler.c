#include "can_handler.h"

#include "board_io.h"
#include "can_bus.h"
#include "gps.h"
#include "message_handler.h"

static void can_tx_send_gps_data(void) {
  static dbc_GPS_NAV_s gps_struct;
  can__msg_t can_msg = {};
  dbc_message_header_t header;

  gps_coordinates_t data = gps__get_coordinates();

  gps_struct.GPS_NAV_latitude = data.latitude;
  gps_struct.GPS_NAV_longitude = data.longitude;

  header = dbc_encode_GPS_NAV(can_msg.data.bytes, &gps_struct);
  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  can__tx(can1, &can_msg, 0);
}

static void can_tx_send_sensor_sonar_data(void) {
  static dbc_SENSOR_SONARS_s sensor_struct;
  can__msg_t can_msg = {};
  dbc_message_header_t header;

  header = dbc_encode_SENSOR_SONARS(can_msg.data.bytes, &sensor_struct);
  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  can__tx(can1, &can_msg, 0);

  sensor_struct.SENSOR_SONARS_left += 1;
  sensor_struct.SENSOR_SONARS_middle += 2;
  sensor_struct.SENSOR_SONARS_right += 3;
}

static void can_tx_send_brake_light_data(void) {
  static dbc_BRAKE_LIGHTS_s brake_struct;
  can__msg_t can_msg = {};
  dbc_message_header_t header;

  header = dbc_encode_BRAKE_LIGHTS(can_msg.data.bytes, &brake_struct);
  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  brake_struct.BRAKE_LIGHTS_on = gpio__get(board_io__get_sw3());
  can__tx(can1, &can_msg, 0);
}

void can_handler__transmit_messages_10hz(void) {
  can_tx_send_sensor_sonar_data();
  can_tx_send_brake_light_data();
  can_tx_send_gps_data();
}

void can_handler__handle_all_incoming_messages(void) {
  can__msg_t can_msg = {};

  while (can__rx(can1, &can_msg, 0)) {
    // Construct "message header" that we need for the decode_*() API
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };

    if (can_msg.msg_id == dbc_header_SENSOR_SONARS.message_id) {
      handle_sensor_sonars_message(header, can_msg.data.bytes);
    } else if (can_msg.msg_id == dbc_header_BRAKE_LIGHTS.message_id) {
      handle_brake_lights_message(header, can_msg.data.bytes);
    } else if (can_msg.msg_id == dbc_header_GPS_NAV.message_id) {
      handle_gps_nav_message(header, can_msg.data.bytes);
    }
  }
}
