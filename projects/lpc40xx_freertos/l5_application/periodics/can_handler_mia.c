#include "can_handler_mia.h"
#include "board_io.h"
#include "gpio.h"
#include "message_handler.h"

#include <stdio.h>

void can_handler__manage_mia_10hz() {
  // We are in 10hz slot, so increment MIA counter by 100ms
  const uint32_t mia_increment_value = 100;
  dbc_SENSOR_SONARS_s sensor_msg = message_handler__get_sensor_sonars_data();

  if (dbc_service_mia_SENSOR_SONARS(&sensor_msg, mia_increment_value)) {
    // Turn off LED to indicate sonar sensor MIA
    gpio__set(board_io__get_led0());
  }
  message_handler__set_sensor_sonars_data(sensor_msg);
}