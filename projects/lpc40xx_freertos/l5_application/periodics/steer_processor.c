#include "steer_processor.h"

#define THRESHOLD 50 // 50 cm threshold

void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm) {
  if (left_sensor_cm < THRESHOLD && right_sensor_cm < THRESHOLD) {
    if (left_sensor_cm < right_sensor_cm) {
      steer_right();
    } else {
      steer_left();
    }
  } else if (left_sensor_cm < THRESHOLD && right_sensor_cm > THRESHOLD) {
    steer_right();
  } else if (left_sensor_cm > THRESHOLD && right_sensor_cm < THRESHOLD) {
    steer_left();
  } else {
    return;
  }
}