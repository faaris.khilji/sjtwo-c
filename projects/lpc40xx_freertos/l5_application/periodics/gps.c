// gps.c

#include "gps.h"

// GPS module dependency
#include "gpio.h"
#include "line_buffer.h"
#include "uart.h"

#include "clock.h" // needed for UART initialization

#include <stdlib.h>
#include <string.h>

#define LONGITUDE_DATA_IDX 4
#define LATITUDE_DATA_IDX 2

// Change this according to which UART you plan to use
static const uart_e gps_uart = UART__3;

// Space for the line buffer, and the line buffer data structure instance
static char line_buffer[200];
static line_buffer_s line;

static gps_coordinates_t parsed_coordinates;

static void gps__transfer_data_from_uart_driver_to_line_buffer(void) {
  char byte;
  const uint32_t zero_timeout = 0;

  while (uart__get(gps_uart, &byte, zero_timeout)) {
    line_buffer__add_byte(&line, byte);
  }
}

static void gps__parse_coordinates_from_line(void) {
  char gps_line[200];
  uint8_t data_idx = 0;
  gps_coordinates_t temp_coord = {0};

  if (line_buffer__remove_line(&line, gps_line, sizeof(gps_line))) {
    char *unused_ptr;
    char *tok = strtok_r(gps_line, ",", &unused_ptr);

    if (!strcmp(tok, "$GPGGA")) {
      while (tok != NULL) {
        if (data_idx == LATITUDE_DATA_IDX) {
          temp_coord.latitude = strtof(tok, NULL);
        } else if (data_idx == LONGITUDE_DATA_IDX) {
          temp_coord.longitude = strtof(tok, NULL);
        } else if (data_idx == LATITUDE_DATA_IDX + 1) {
          temp_coord.latitude *= !strcmp(tok, "N") ? 1 : -1;
          parsed_coordinates.latitude = temp_coord.latitude;
        } else if (data_idx == LONGITUDE_DATA_IDX + 1) {
          temp_coord.longitude *= !strcmp(tok, "E") ? 1 : -1;
          parsed_coordinates.longitude = temp_coord.longitude;
          break;
        }
        data_idx++;
        tok = strtok_r(NULL, ",", &unused_ptr);
      }
    }
  }
}

void gps__init(void) {
  line_buffer__init(&line, line_buffer, sizeof(line_buffer));
  uart__init(gps_uart, clock__get_peripheral_clock_hz(), 38400);

  gpio__construct_with_function(GPIO__PORT_4, 28, GPIO__FUNCTION_2);
  gpio__construct_with_function(GPIO__PORT_4, 29, GPIO__FUNCTION_2);

  // RX queue should be sized such that can buffer data in UART driver until gps__run_once() is called
  // Note: Assuming 38400bps, we can get 4 chars per ms, and 40 chars per 10ms (100Hz)
  QueueHandle_t rxq_handle = xQueueCreate(100, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(8, sizeof(char)); // We don't send anything to the GPS
  uart__enable_queues(gps_uart, rxq_handle, txq_handle);
  parsed_coordinates.latitude = 0;
  parsed_coordinates.longitude = 0;
}

void gps__run_once(void) {
  gps__transfer_data_from_uart_driver_to_line_buffer();
  gps__parse_coordinates_from_line();
}

gps_coordinates_t gps__get_coordinates(void) { return parsed_coordinates; }
