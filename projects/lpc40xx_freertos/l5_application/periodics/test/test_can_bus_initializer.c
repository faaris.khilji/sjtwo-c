#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockcan_bus.h"

// Include the source we wish to test
#include "can_bus_initializer.h"

void setUp(void) {}

void test_can_init_can1(void) {
  uint32_t baudrate_kbps = 1000;
  uint16_t rxq_size = 100;
  uint16_t txq_size = 100;
  can__init_ExpectAndReturn(can1, baudrate_kbps, rxq_size, txq_size, NULL, NULL, true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can1);
  TEST_ASSERT_TRUE(can_bus_initializer_run_once(can1, baudrate_kbps, rxq_size, txq_size));
}

void test_can_init_can2(void) {
  uint32_t baudrate_kbps = 500;
  uint16_t rxq_size = 50;
  uint16_t txq_size = 100;
  can__init_ExpectAndReturn(can2, baudrate_kbps, rxq_size, txq_size, NULL, NULL, true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can2);
  TEST_ASSERT_TRUE(can_bus_initializer_run_once(can2, baudrate_kbps, rxq_size, txq_size));
}

void test_can_init_invalid_baudrate(void) {
  uint32_t baudrate_kbps = 3000000;
  uint16_t rxq_size = 100;
  uint16_t txq_size = 50;
  TEST_ASSERT_FALSE(can_bus_initializer_run_once(can1, baudrate_kbps, rxq_size, txq_size));
}

void test_can_init_incorrect_can(void) {
  uint32_t baudrate_kbps = 250;
  uint16_t rxq_size = 50;
  uint16_t txq_size = 100;
  TEST_ASSERT_FALSE(can_bus_initializer_run_once(can_max, baudrate_kbps, rxq_size, txq_size));
}

void test_can_init_invalid_rxq_size(void) {
  uint32_t baudrate_kbps = 500;
  uint16_t rxq_size = 0;
  uint16_t txq_size = 100;
  TEST_ASSERT_FALSE(can_bus_initializer_run_once(can2, baudrate_kbps, rxq_size, txq_size));
}

void test_can_init_invalid_txq_size(void) {
  uint32_t baudrate_kbps = 500;
  uint16_t rxq_size = 100;
  uint16_t txq_size = 0;
  TEST_ASSERT_FALSE(can_bus_initializer_run_once(can2, baudrate_kbps, rxq_size, txq_size));
}