#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "project.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockgpio.h"
#include "Mockgps.h"
// Include the source we wish to test
#include "Mockmessage_handler.h"
#include "can_handler.h"

void setUp(void) {}

void tearDown(void) {}

void test_can_handler__transmit_messages_10hz(void) {
  gpio_s gpio = {};
  gps_coordinates_t gps_data;
  can__tx_ExpectAnyArgsAndReturn(1);
  board_io__get_sw3_ExpectAndReturn(gpio);
  gpio__get_ExpectAnyArgsAndReturn(0);
  can__tx_ExpectAnyArgsAndReturn(1);
  gps__get_coordinates_ExpectAndReturn(gps_data);
  can__tx_ExpectAnyArgsAndReturn(1);
  can_handler__transmit_messages_10hz();
}

void test_can_handler__handle_all_incoming_messages_SENSOR_SONAR_MSG(void) {
  can__msg_t can_msg = {.msg_id = dbc_header_SENSOR_SONARS.message_id};
  dbc_message_header_t header;
  can__rx_ExpectAnyArgsAndReturn(1);
  can__rx_ReturnThruPtr_can_message_ptr(&can_msg);
  handle_sensor_sonars_message_ExpectAnyArgs();
  can__rx_ExpectAnyArgsAndReturn(0);
  can_handler__handle_all_incoming_messages();
}

void test_can_handler__handle_all_incoming_messages_BRAKE_LIGHTS_MSG(void) {
  can__msg_t can_msg = {.msg_id = dbc_header_BRAKE_LIGHTS.message_id};
  dbc_message_header_t header;
  can__rx_ExpectAnyArgsAndReturn(1);
  can__rx_ReturnThruPtr_can_message_ptr(&can_msg);
  handle_brake_lights_message_ExpectAnyArgs();
  can__rx_ExpectAnyArgsAndReturn(0);
  can_handler__handle_all_incoming_messages();
}

void test_can_handler__handle_all_incoming_messages_GPS_NAV_MSG(void) {
  can__msg_t can_msg = {.msg_id = dbc_header_GPS_NAV.message_id};
  dbc_message_header_t header;
  can__rx_ExpectAnyArgsAndReturn(1);
  can__rx_ReturnThruPtr_can_message_ptr(&can_msg);
  handle_gps_nav_message_ExpectAnyArgs();
  can__rx_ExpectAnyArgsAndReturn(0);
  can_handler__handle_all_incoming_messages();
}
