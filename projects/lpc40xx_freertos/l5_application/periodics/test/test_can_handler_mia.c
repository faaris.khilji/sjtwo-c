#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockcan_handler.h"
#include "Mockgpio.h"
#include "Mockgps.h"
#include "Mockmessage_handler.h"
// Include the source we wish to test
#include "can_handler_mia.h"
#include "project.h"

const uint32_t dbc_mia_threshold_SENSOR_SONARS = 150;

const dbc_SENSOR_SONARS_s dbc_mia_replacement_SENSOR_SONARS = {
    .SENSOR_SONARS_left = 100, .SENSOR_SONARS_middle = 100, .SENSOR_SONARS_right = 100};

void setUp(void) {}

void tearDown(void) {}

void test_can_handler__manage_mia_10hz(void) {
  dbc_SENSOR_SONARS_s sensor_msg;
  message_handler__get_sensor_sonars_data_ExpectAndReturn(sensor_msg);
  message_handler__set_sensor_sonars_data_ExpectAnyArgs();
  can_handler__manage_mia_10hz();
}