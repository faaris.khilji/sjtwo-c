
#include "unity.h"
#include <string.h>

// Mocks
#include "Mockclock.h"
#include "Mockgpio.h"
#include "Mockuart.h"

#include "Mockqueue.h"

// We can choose to use real implementation (not Mock) for line_buffer.h
// because this is a relatively trivial module
#include "line_buffer.h"

// Include the source we wish to test
#include "gps.h"

gps_coordinates_t gps_coordinates;

void setUp(void) {
  gpio_s ret;
  clock__get_peripheral_clock_hz_ExpectAndReturn(96000000);
  uart__init_Expect(UART__3, 0, 38400);
  uart__init_IgnoreArg_peripheral_clock();

  gpio__construct_with_function_ExpectAnyArgsAndReturn(ret);
  gpio__construct_with_function_ExpectAnyArgsAndReturn(ret);

  xQueueCreate_ExpectAnyArgsAndReturn(NULL);
  xQueueCreate_ExpectAnyArgsAndReturn(NULL);
  uart__enable_queues_ExpectAnyArgsAndReturn(true);

  gps__init();

  gps_coordinates.latitude = 0;
  gps_coordinates.longitude = 0;
}
void tearDown(void) {}

void test_init(void) {
  gpio_s ret;
  clock__get_peripheral_clock_hz_ExpectAndReturn(960000000);
  uart__init_Expect(UART__3, 0, 38400);
  uart__init_IgnoreArg_peripheral_clock();

  gpio__construct_with_function_ExpectAnyArgsAndReturn(ret);
  gpio__construct_with_function_ExpectAnyArgsAndReturn(ret);

  xQueueCreate_ExpectAnyArgsAndReturn(NULL);
  xQueueCreate_ExpectAnyArgsAndReturn(NULL);
  uart__enable_queues_ExpectAnyArgsAndReturn(true);

  gps__init();
}

void test_GPGLL_line_is_ignored(void) {
  const char *uart_driver_returned_data =
      "$GPGLL,112233.00,1234.56,N,9876.54,E,1,03,1.5,280.2,M,-34.0,M,x.x,xxxx*75\r\n";
  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const char the_char_to_return = uart_driver_returned_data[index];

    const bool last_char = (index < strlen(uart_driver_returned_data));
    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);

    // TODO: Research on ReturnThruPtr() to make it return the char 'the_char_to_return'
    uart__get_IgnoreArg_input_byte();
    uart__get_IgnoreArg_timeout_ms();
    uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
  }

  gps__run_once();

  // TODO: Test gps__get_coordinates():
  gps_coordinates = gps__get_coordinates();

  TEST_ASSERT_EQUAL(0, gps_coordinates.latitude);
  TEST_ASSERT_EQUAL(0, gps_coordinates.longitude);
}

void test_GPGGA_coordinates_are_parsed(void) {
  const char *uart_driver_returned_data =
      "$GPGGA,112233.00,1234.56,N,9876.54,E,1,03,1.5,280.2,M,-34.0,M,x.x,xxxx*75\r\n";
  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const char the_char_to_return = uart_driver_returned_data[index];

    const bool last_char = (index < strlen(uart_driver_returned_data));
    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    // TODO: Research on ReturnThruPtr() to make it return the char 'the_char_to_return'
    uart__get_IgnoreArg_input_byte();
    uart__get_IgnoreArg_timeout_ms();
    uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
  }

  gps__run_once();

  // TODO: Test gps__get_coordinates():
  gps_coordinates = gps__get_coordinates();

  TEST_ASSERT_EQUAL(1234.56, gps_coordinates.latitude);
  TEST_ASSERT_EQUAL(9876.54, gps_coordinates.longitude);
}

void test_GPGGA_incomplete_line(void) {
  const char *uart_driver_returned_data = "$GPGGA,112233.00,1234.56,N,9876.54,E,1,03,1.5,280.2,M,-34.0,M,x.x,xxxx*75";
  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const char the_char_to_return = uart_driver_returned_data[index];

    const bool last_char = (index < strlen(uart_driver_returned_data));
    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    // TODO: Research on ReturnThruPtr() to make it return the char 'the_char_to_return'
    uart__get_IgnoreArg_input_byte();
    uart__get_IgnoreArg_timeout_ms();
    uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
  }

  gps__run_once();

  gps_coordinates = gps__get_coordinates();

  TEST_ASSERT_EQUAL(0, gps_coordinates.latitude);
  TEST_ASSERT_EQUAL(0, gps_coordinates.longitude);
}

void test_GPGGA_coordinates_are_negative(void) {
  const char *uart_driver_returned_data = "$GPGGA,112233.00,1234.56,S,9876.54,W,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh\r\n";
  for (size_t index = 0; index <= strlen(uart_driver_returned_data); index++) {
    const char the_char_to_return = uart_driver_returned_data[index];

    const bool last_char = (index < strlen(uart_driver_returned_data));
    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    // TODO: Research on ReturnThruPtr() to make it return the char 'the_char_to_return'
    uart__get_IgnoreArg_input_byte();
    uart__get_IgnoreArg_timeout_ms();
    uart__get_ReturnThruPtr_input_byte(&uart_driver_returned_data[index]);
  }

  gps__run_once();

  // TODO: Test gps__get_coordinates():
  gps_coordinates = gps__get_coordinates();

  TEST_ASSERT_EQUAL(-1234.56, gps_coordinates.latitude);
  TEST_ASSERT_EQUAL(-9876.54, gps_coordinates.longitude);
}

void test_more_that_you_think_you_need(void) {}
