#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "project.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockgpio.h"

#include "message_handler.h"

void setUp(void) {}

void tearDown(void) {}

void test_handle_sensors_sonars_message(void) {
  dbc_message_header_t header;
  const uint8_t bytes[8];
  gpio_s led;
  board_io__get_led0_ExpectAndReturn(led);
  gpio__reset_ExpectAnyArgs();
  handle_sensor_sonars_message(header, bytes);
}

void test_handle_brake_lights_message_light_on(void) {
  dbc_message_header_t header;
  uint8_t bytes[8];
  bytes[0] = 0x1;
  header = dbc_header_BRAKE_LIGHTS;
  gpio_s led;
  board_io__get_led3_ExpectAndReturn(led);
  gpio__reset_ExpectAnyArgs();
  handle_brake_lights_message(header, bytes);
}

void test_handle_brake_lights_message_light_off(void) {
  dbc_message_header_t header;
  uint8_t bytes[8];
  bytes[0] = 0x0;
  header = dbc_header_BRAKE_LIGHTS;
  gpio_s led;
  board_io__get_led3_ExpectAndReturn(led);
  gpio__set_ExpectAnyArgs();
  handle_brake_lights_message(header, bytes);
}

void test_handle_gps_nav_message(void) {
  dbc_message_header_t header;
  const uint8_t bytes[8];
  handle_gps_nav_message(header, bytes);
}