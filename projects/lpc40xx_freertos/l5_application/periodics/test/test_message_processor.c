#include "unity.h"

#include "Mockmessage.h"

#include "message_processor.h"

static bool message__read_stub(message_s *message_to_read, int call_count) {
  bool message_was_read = false;
  printf("call_count: %d", call_count);

  if (call_count >= 2) {
    message_was_read = false;
  } else {
    message_was_read = true;
  }

  if (call_count == 0) {
    message_to_read->data[0] = '$';
  }
  if (call_count == 1) {
    message_to_read->data[1] = 'X';
  }

  return message_was_read;
}

static bool message__read_stub_no_dollar_sign(message_s *message_to_read, int call_count) {
  bool message_was_read = false;

  if (call_count >= 2) {
    message_was_read = false;
  } else {
    message_was_read = true;
  }

  if (call_count == 0) {
    message_to_read->data[0] = 'X';
  }
  if (call_count == 1) {
    message_to_read->data[1] = 'X';
  }

  return message_was_read;
}

static bool message__read_stub_message_read_fail(message_s *message_to_read, int call_count) { return false; }

// This only tests if we process at most 3 messages
void test_process_3_messages(void) {
  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  // Third time when message_read() is called, we will return false to break the loop
  message__read_ExpectAndReturn(NULL, false);
  message__read_IgnoreArg_message_to_read();

  // Since we did not return a message that starts with '$' this should return false
  TEST_ASSERT_FALSE(message_processor());
}

void test_process_message_with_dollar_sign(void) {
  message__read_StubWithCallback(message__read_stub);

  // Function under test
  TEST_ASSERT_TRUE(message_processor());
}

void test_process_messages_without_any_dollar_sign(void) {
  message__read_StubWithCallback(message__read_stub_no_dollar_sign);

  // Function under test
  TEST_ASSERT_FALSE(message_processor());
}
