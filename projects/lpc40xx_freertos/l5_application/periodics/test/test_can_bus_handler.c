#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockacceleration.h"
#include "Mockcan_bus.h"
//#include "Mockcan_led_handler.h"
#include "Mockgpio.h"

// Include the source we wish to test
#include "can_bus_handler.h"

void setUp(void) {}

void tearDown(void) {}

// void test_incorrect_can_input(void) { TEST_ASSERT_FALSE(can_handler_handle_button_press(can_max)); }

void test_button_press_run_once_message_received(void) {
  can__msg_t can_message = {};
  can_message.msg_id = 0x01;
  can_message.frame_fields.is_29bit = 0;
  can_message.frame_fields.data_len = 2;
  can__tx_ExpectAndReturn(can1, &can_message, 0, true);

  gpio_s switch_2 = {GPIO__PORT_0, 30};
  gpio__get_ExpectAndReturn(switch_2, true);
  gpio__get_IgnoreArg_gpio();

  TEST_ASSERT_TRUE(can_handler_send_button_press(can1));
  can_message.data.qword = 0x00;

  gpio_s led_2 = {GPIO__PORT_1, 18};
  gpio__reset_Expect(led_2);
  gpio__reset_IgnoreArg_gpio();

  can__rx_ExpectAndReturn(can1, &can_message, 0, true);
  can__rx_IgnoreArg_can_message_ptr();
  can__rx_ReturnThruPtr_can_message_ptr(&can_message);
  can__rx_ExpectAndReturn(can1, &can_message, 0, false);

  can_handler_handle_button_press(can1);
}

void test_button_not_pressed_message_received(void) {

  can__msg_t can_message = {};
  can_message.msg_id = 0x01;
  can_message.frame_fields.is_29bit = 0;
  can_message.frame_fields.data_len = 2;
  can_message.data.qword = 0xAA;

  gpio_s switch_2 = {GPIO__PORT_0, 30};
  gpio__get_ExpectAndReturn(switch_2, false);
  gpio__get_IgnoreArg_gpio();

  can__tx_ExpectAndReturn(can1, &can_message, 0, true);
  TEST_ASSERT_TRUE(can_handler_send_button_press(can1));

  gpio_s led_2 = {GPIO__PORT_1, 18};
  gpio__set_Expect(led_2);
  gpio__set_IgnoreArg_gpio();

  can__rx_ExpectAndReturn(can1, &can_message, 0, true);
  can__rx_IgnoreArg_can_message_ptr();
  can__rx_ReturnThruPtr_can_message_ptr(&can_message);
  can__rx_ExpectAndReturn(can1, &can_message, 0, false);

  can_handler_handle_button_press(can1);
}

void test_can_message_handler_button_not_pressed(void) {

  can__msg_t can_message = {};
  can_message.msg_id = BUTTON_STATUS_MSGID;
  can_message.frame_fields.is_29bit = 0;
  can_message.frame_fields.data_len = 2;
  can_message.data.qword = 0xAA;

  gpio_s switch_2 = {GPIO__PORT_0, 30};
  gpio__get_ExpectAndReturn(switch_2, false);
  gpio__get_IgnoreArg_gpio();

  can__tx_ExpectAndReturn(can1, &can_message, 0, true);
  TEST_ASSERT_TRUE(can_handler_send_button_press(can1));

  gpio_s led_2 = {GPIO__PORT_1, 18};
  gpio__set_Expect(led_2);
  gpio__set_IgnoreArg_gpio();

  can__rx_ExpectAndReturn(can1, &can_message, 0, true);
  can__rx_IgnoreArg_can_message_ptr();
  can__rx_ReturnThruPtr_can_message_ptr(&can_message);
  can__rx_ExpectAndReturn(can1, &can_message, 0, false);

  can_handler_handle_messages(can1);
}

void test_can_message_handler_accel_msg(void) {
  can__msg_t can_message = {};
  can_message.msg_id = ACCEL_DATA_MSGID;
  can_message.frame_fields.is_29bit = 0;
  can_message.frame_fields.data_len = 6;

  can_message.data.words[0] = 0;
  can_message.data.words[1] = 1;
  can_message.data.words[2] = 2;

#if USE_ACCEL_PHYS_SENSOR
  acceleration__axis_data_s acceleration_data;
  acceleration_data.x = 0;
  acceleration_data.y = 1;
  acceleration_data.z = 2;
  acceleration__get_data_ExpectAndReturn(acceleration_data);
#endif

  can__tx_ExpectAndReturn(can1, &can_message, 0, true);
  TEST_ASSERT_TRUE(can_handler_send_accelerator_data(can1));

  can__rx_ExpectAndReturn(can1, &can_message, 0, true);
  can__rx_IgnoreArg_can_message_ptr();
  can__rx_ReturnThruPtr_can_message_ptr(&can_message);
  can__rx_ExpectAndReturn(can1, &can_message, 0, false);

  gpio__set_ExpectAnyArgs();
  gpio__reset_ExpectAnyArgs();
  gpio__reset_ExpectAnyArgs();

  TEST_ASSERT_EQUAL(can_message.msg_id, ACCEL_DATA_MSGID);
  TEST_ASSERT_TRUE(can_handler_handle_messages(can1));
}

void test_can_led_turn_off_lowest(void) {
#if USE_ACCEL_PHYS_SENSOR
  uint16_t x = 0;
  uint16_t y = 1;
  uint16_t z = 2;
  gpio_s ledx;
  gpio_s ledy;
  gpio_s ledz;
  gpio__set_ExpectAnyArgs();
  gpio__reset_ExpectAnyArgs();
  gpio__reset_ExpectAnyArgs();
  can_accel_led_turn_off_lowest_value(x, y, z);
#endif
}