#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockgpio.h"

// Include the source we wish to test
#include "Mockswitch_led_logic.h"
#include "periodic_callbacks.h"

#include "Mockacceleration.h"
#include "Mockcan_bus.h"
#include "Mockcan_bus_initializer.h"
#include "Mockcan_handler.h"
#include "Mockcan_handler_mia.h"
#include "Mockfake_gps.h"
#include "Mockgps.h"
#include "Mockmessage_handler.h"

#include "project.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  gps__init_Expect();
  fake_gps__init_Expect();
  can_bus_initializer_run_once_ExpectAnyArgsAndReturn(1);
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  gpio_s gpio = {};
  gps_coordinates_t coord;
  dbc_SENSOR_SONARS_s sensor_data;
  gps_coordinates_t gps_data;
  fake_gps__run_once_Expect();
  // board_io__get_led1_ExpectAndReturn(gpio);
  // gpio__toggle_Expect(gpio);
  gps__get_coordinates_ExpectAndReturn(coord);
  can__is_bus_off_ExpectAndReturn(0, 0);
  message_handler__get_sensor_sonars_data_ExpectAndReturn(sensor_data);
  message_handler__get_gps_data_ExpectAndReturn(gps_data);
  periodic_callbacks__1Hz(0);
}

void test__periodic_callbacks__10Hz(void) {
  // can_handler__transmit_messages_10hz_Expect();
  can_handler__handle_all_incoming_messages_Expect();
  can_handler__manage_mia_10hz_Expect();
  periodic_callbacks__10Hz(0);
}

void test__periodic_callbacks__100Hz(void) {
  gpio_s gpio = {};
  gps__run_once_Expect();
  periodic_callbacks__100Hz(0);
}