#pragma once

#include <stdint.h>

#include "gps.h"
#include "project.h"

void handle_sensor_sonars_message(dbc_message_header_t header, const uint8_t bytes[8]);
void handle_brake_lights_message(dbc_message_header_t header, const uint8_t bytes[8]);
void handle_gps_nav_message(dbc_message_header_t header, const uint8_t bytes[8]);
dbc_SENSOR_SONARS_s message_handler__get_sensor_sonars_data(void);
gps_coordinates_t message_handler__get_gps_data(void);

// To be used by can mia manager
void message_handler__set_sensor_sonars_data(dbc_SENSOR_SONARS_s data);