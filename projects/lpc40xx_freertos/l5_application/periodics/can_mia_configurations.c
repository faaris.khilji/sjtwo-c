#include <stdint.h>

#include "project.h"

const uint32_t dbc_mia_threshold_SENSOR_SONARS = 150;

const dbc_SENSOR_SONARS_s dbc_mia_replacement_SENSOR_SONARS = {
    .SENSOR_SONARS_left = 100, .SENSOR_SONARS_middle = 100, .SENSOR_SONARS_right = 100};