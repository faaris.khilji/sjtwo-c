#include <stdio.h>

#include "periodic_callbacks.h"

#include "board_io.h"
#include "gpio.h"

#include "switch_led_logic.h"

#include "FreeRTOS.h"
#include "task.h"

#include "can_bus_initializer.h"
#include "can_handler.h"
#include "can_handler_mia.h"
#include "fake_gps.h"
#include "gps.h"
#include "message_handler.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  gps__init();
  fake_gps__init();
  can_bus_initializer_run_once(can1, 100, 200, 200);
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  fake_gps__run_once();
  // gpio__toggle(board_io__get_led1());
  gps_coordinates_t data = gps__get_coordinates();
  dbc_SENSOR_SONARS_s sensor_data = message_handler__get_sensor_sonars_data();
  gps_coordinates_t gps_data = message_handler__get_gps_data();

  printf("sensor data (l,m,r): %d, %d, %d\n", sensor_data.SENSOR_SONARS_left, sensor_data.SENSOR_SONARS_middle,
         sensor_data.SENSOR_SONARS_right);

  printf("gps data (la/lo): %f, %f\n", gps_data.latitude, gps_data.longitude);

  if (can__is_bus_off(can1)) {
    can__reset_bus(can1);
  }
}

void periodic_callbacks__10Hz(uint32_t callback_count) {

  // can_handler__transmit_messages_10hz(); //Uncomment if board is to be the transmitter

  can_handler__handle_all_incoming_messages();
  can_handler__manage_mia_10hz();
}

void periodic_callbacks__100Hz(uint32_t callback_count) {
  gps__run_once();
  // gpio__toggle(board_io__get_led2());
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  // gpio__toggle(board_io__get_led3());
  // Add your code here
}
